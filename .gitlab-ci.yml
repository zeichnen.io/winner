# GitLab CI builds are executed on docker containers
# We're taking the Amazon Java 8 image here, because it preserves JavaFX in
# the form we need it
image: amazoncorretto:8u222

# Include CodeClimate quality testing
include:
  - template: Code-Quality.gitlab-ci.yml

stages:
  - build
  - test

# Making Gradle executable and setting env variables for it
before_script:
  - chmod -R +x *
  - export GRADLE_USER_HOME=`pwd`/.gradle

# Caching Gradle builds to speed test tasks up
cache:
  paths:
    - .gradle/wrapper
    - .gradle/caches
    - .gradle/build-cache

# Build the JAR and keep it for a month
gradle_build:
  stage: build
  script:
    - ./gradlew assemble
  artifacts:
    paths:
      - build/libs/*.jar
    expire_in: 30 days

# Publish latex documentation to https://zeichnen.io.gitlab.io/winner/winner.pdf
pages:
  image: blang/latex
  stage: build
  variables:
    # We have to compile latex 3 times so the TOC will be generated and the references set
    LATEX_COMPILE_COUNT: 3
  script:
    - mkdir public/
    - cd doc/
    - for i in $(seq 1 $LATEX_COMPILE_COUNT); do for n in $(find . -iname '*.tex'); do pdflatex -interaction=nonstopmode "$n"; cp *.pdf ../public/; done; done
  # Needed for publishing it
  artifacts:
    paths:
      - public
  # Only run this job if something in 'doc' has changed and we're on master
  rules:
    - if: '$CI_COMMIT_REF_NAME == "master"'
      changes:
      - doc/*
      when: always

unit_test:
  stage: test
  script:
    - ./gradlew test
  # Upload unit-test results on gitlab
  artifacts:
    reports:
      junit: build/test-results/test/TEST-*.xml

integration_test:
  stage: test
  variables:
    SHELL_ONLY_TESTS: "true"
  script:
    - amazon-linux-extras install corretto8 # Needed for integration tests
    - ./gradlew integrationTest
  # Upload integration-test results on gitlab
  artifacts:
    reports:
      junit: build/test-results/integrationTest/TEST-*.xml

code_quality:
  only:
    - merge_requests