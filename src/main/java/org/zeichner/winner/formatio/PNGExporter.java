package org.zeichner.winner.formatio;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import org.zeichner.winner.ui.fields.drawfield.DrawField;
import org.zeichner.winner.ui.fields.menufield.MenuField;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class PNGExporter {

    private final FileChooser fileChooser;
    private final String pngFormat = "png";

    // region Simpleton-Stuff
    private final static PNGExporter INSTANCE = new PNGExporter();

    private PNGExporter() {
        this.fileChooser = new FileChooser();
        this.fileChooser.getExtensionFilters().add(Extensions.pngExtensionFilter);
    }

    public static PNGExporter getInstance() {
        return INSTANCE;
    }
    // endregion

    public void pngExport() {

        File file = this.fileChooser.showSaveDialog(MenuField.getInstance().getStage().getOwner());
        WritableImage image = DrawField.getInstance().getCanvas().snapshot(new SnapshotParameters(), null);

        try {
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), pngFormat, file);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
