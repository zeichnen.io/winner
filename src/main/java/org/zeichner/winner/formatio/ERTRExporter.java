package org.zeichner.winner.formatio;

import javafx.stage.FileChooser;
import org.zeichner.winner.ui.fields.menufield.MenuField;
import org.zeichner.winner.ui.fields.textfield.TextField;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class ERTRExporter {

    private final FileChooser fileChooser;

    // region Simpleton-Stuff
    private final static ERTRExporter INSTANCE = new ERTRExporter();

    private ERTRExporter() {
        this.fileChooser = new FileChooser();
        this.fileChooser.getExtensionFilters().add(Extensions.wintxtExtensionFilter);
    }

    public static ERTRExporter getInstance() {
        return INSTANCE;
    }
    // endregion

    public void ertrExport() {

        Path pathToFile = this.fileChooser.showSaveDialog(MenuField.getInstance().getStage().getOwner()).toPath();
        String content = TextField.getInstance().getCodeArea().getText();
        try {
            Files.write(pathToFile, content.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
