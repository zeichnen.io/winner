package org.zeichner.winner.formatio;

import javafx.stage.FileChooser;

/**
 * In here we store the different extensions we want to be compatible with our program.
 */
public final class Extensions {
    private final static String ERTR_EXTENSION_NAME = "*.wintxt";
    private final static String PNG_EXTENSION_NAME = "*.png";
    public final static FileChooser.ExtensionFilter wintxtExtensionFilter = new FileChooser.ExtensionFilter("Winner Txt-Format (" + ERTR_EXTENSION_NAME + ")", ERTR_EXTENSION_NAME);
    public final static FileChooser.ExtensionFilter pngExtensionFilter = new FileChooser.ExtensionFilter("PNG-Format (" + PNG_EXTENSION_NAME + ")", PNG_EXTENSION_NAME);
}
