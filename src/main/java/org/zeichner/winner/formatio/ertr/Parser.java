package org.zeichner.winner.formatio.ertr;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import lombok.Getter;

import java.util.Arrays;
import java.util.OptionalInt;

public class Parser {
    private final Grammar[] grammars;

    /**
     * A modifiable list containing the currently active {@link ParserCallbacks.Success success callbacks}.
     * */
    @Getter
    private ObservableList<ParserCallbacks.Success> successCallbacks;

    /**
     * A modifiable list containing the currently active {@link ParserCallbacks.Failure failure callbacks}.
     */
    @Getter
    private ObservableList<ParserCallbacks.Failure> failureCallbacks;

    public Parser(Grammar[] grammars){
        // Creating a non-mutable copy here
        this.grammars = Arrays.copyOf(grammars, grammars.length);

        this.successCallbacks = FXCollections.observableArrayList();
        this.failureCallbacks = FXCollections.observableArrayList();
    }

    /**
     * Parses the given string invoking the set parser callbacks on success
     * and failure. The success callback is invoked as soon as a new token
     * has been successfully parsed, and the failure callback as soon as it
     * is clear when the next valid token starts.
     */
    public void parse(String src){
        OptionalInt startErrIdx = OptionalInt.empty();

        // Prepare a last and current idx (needed for error handling)
        int lastIdx = 0;
        int idx = skipWhitespaces(src, 0);

        for(; idx < src.length(); lastIdx = idx, idx = skipWhitespaces(src, ++idx)){
            // Try to match the next token at the current position and get the
            // position of the last character
            OptionalInt newIdx = tryToMatchToken(src, idx);

            // If we found a new token ...
            if(newIdx.isPresent()){
                // Call the error callback if the previous characters were invalid
                final int errorEndIdx = lastIdx;
                startErrIdx.ifPresent(i -> callFailureCallback(src, i, errorEndIdx));
                startErrIdx = OptionalInt.empty();

                idx = newIdx.getAsInt();
            }
            // If we couldn't match a token at the current position, and the error
            // start index is not set, then set it to the current position
            else if(!startErrIdx.isPresent()){
                startErrIdx = OptionalInt.of(idx);
            }
        }

        // Call the error callback if the previous characters were invalid
        startErrIdx.ifPresent(i -> callFailureCallback(src, i, src.length() - 1));
    }

    private OptionalInt tryToMatchToken(String src, int idx){
        for (Grammar grammar : grammars) {
            int newIdx = grammar.callManyMatch(src, idx, successCallbacks);
            if (newIdx != -1) {
                return OptionalInt.of(newIdx - 1);
            }
        }
        return OptionalInt.empty();
    }

    private void callFailureCallback(String src, int start, int end){
        failureCallbacks.forEach(c -> c.accept(start, src.substring(start, end + 1)));
    }

    private int skipWhitespaces(String src, int idx){
        for(; idx < src.length(); idx++){
            if(!Character.isWhitespace(src.charAt(idx))){ break; }
        }
        return idx;
    }
}
