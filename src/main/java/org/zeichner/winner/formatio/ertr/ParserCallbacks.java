package org.zeichner.winner.formatio.ertr;

import java.util.List;
import java.util.function.BiConsumer;

public class ParserCallbacks {

    /**
     * Called by the {@link Parser} on successful parse of a statement.
     * The first argument is the parsed grammar, the second one is the
     * list of tokens lexed by it.
     */
    public interface Success extends BiConsumer<Grammar, List<String>> { }

    /**
     * Called by the {@link Parser} when an invalid sequence of characters
     * is encountered in the source. The first argument is the start index
     * in the original source where the error was encountered, the second
     * argument is the whole invalid sequence as a substring of the source.
     */
    public interface Failure extends BiConsumer<Integer, String>{}
}
