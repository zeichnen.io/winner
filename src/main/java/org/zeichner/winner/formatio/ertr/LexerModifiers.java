package org.zeichner.winner.formatio.ertr;

import java.util.Arrays;
import java.util.stream.Collectors;

import static org.zeichner.winner.formatio.ertr.LexerConstants.*;

/**
 * Modifies lexer token matchers, e.g.:
 *
 * <code>LexerModifers.oneOrMore("a")</code> matches one
 * or more <code>a</code>, like
 * <code>aaaaaaaaaaaaaaaaaaaaaaaaaaaaa</code>.
 * */
public class LexerModifiers {
    public static String optional(String t){
        return String.format("(?:%s)?", t);
    }

    public static String oneOrMore(String t){
        return String.format("(?:%s)+", t);
    }

    public static String zeroOrMore(String t){
        return String.format("(?:%s)*", t);
    }

    public static String oneOf(String... alternatives){
        return "(?:" + Arrays
                .stream(alternatives)
                .map(e -> new StringBuilder("(?:").append(e).append(")"))
                .collect(Collectors.joining("|")) + ")";
    }

    /**
     * Forces the given word to match if it's a standalone word, e.g. not part
     * of an other word.
     * */
    public static String standalone(String t){
        // (?<=%1$s|%2$s|^) = (?<=NEWLINE|SPACES|^) - Matches **the following**
        // only if it's preceded by a newline, spaces or the start of input.
        // %3$s = t
        // (?=%1$s|%2$s|$) = (?=NEWLINE|SPACES|$) - Matches **the token before**
        // either if this is a newline, space, or the end of input.
        return String.format("(?<=%1$s|%2$s|^)%3$s(?=%1$s|%2$s|$)", NEWLINE, SPACES, t);
    }

    /**
     * Forces the given string to become a capture group, thus recording it.
     * */
    public static String record(String t){
        return String.format("(%s)", t);
    }

    /**
     * Forces the given string to become a named capture group, thus recording it
     * with the given capture group name.
     * */
    public static String record(String t, String capture){
        return String.format("(?<%s>%s)", capture, t);
    }
}
