package org.zeichner.winner.formatio.ertr;

/**
 * Regexps constant used by the Lexer/Parser.
 */
public class LexerConstants {
    public static final String SPACE = "[ \t]";

    public static final String INTEGER = "[0-9]+";

    public static final String SPACES = SPACE + "+";

    /** Matches an platform-independent line end or the end of input.*/
    public static final String NEWLINE = "(?:\\r\\n|\\r|\\n|$)";

    /** Matches an platform-independent line end.*/
    public static final String NEWLINE_WITHOUT_EOF = "(?:\\r\\n|\\r|\\n)";

    public static final String SPACES_NEWLINE = SPACE + "*" + NEWLINE;

    /** Matches a number-letter name starting with a letter */
    public static final String ENTITY_NAME = "[a-zA-Z][a-zA-Z0-9_]*";

    public static final String WORD = "[a-zA-Z0-9_]+";

    /** Match a word indented by at least one space or tab until the next new line*/
    public static final String INDENTED_WORD = SPACES + WORD + SPACES_NEWLINE;

    /** Matches either '1', 'n' or 'm'*/
    public static final String CARDINALITY = "[1nm]";

    public static final String PRIMARY_KEY = "pk_" + WORD;

    public static final String IS_A_LITERAL = "is a";

    public static final String PART_OF_LITERAL = "part of";

    public static final String WEAK_LITERAL = "weak";

    public static final String ENTITY_LITERAL = "entity";

    public static final String X_LITERAL = "x";

    public static final String AT_LITERAL = "at";
}
