package org.zeichner.winner.formatio.ertr;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.zeichner.winner.formatio.ertr.LexerConstants.*;
import static org.zeichner.winner.formatio.ertr.LexerModifiers.*;

/**
 * A grammar for parsing the <b>E</b>ntity <b>R</b>elations <b>T</b>ext <b>R</b>epresentation.
 */
public enum ERTRGrammar implements Grammar {
    ENTITY(
            // [weak] entity
            optional(record(WEAK_LITERAL) + SPACES), ENTITY_LITERAL, SPACES,
            // <entity_name> [at <coord>x<coord>]
            record(ENTITY_NAME), optional(SPACES), optional(AT_LITERAL + SPACES + record(INTEGER) + X_LITERAL + record(INTEGER)),
            // \n <attributes>
            SPACES_NEWLINE, record(zeroOrMore(INDENTED_WORD))
    ),

    IS_A_RELATION(
            // <entity_name> is a <entity_name>
            record(ENTITY_NAME), SPACES, IS_A_LITERAL, SPACES, record(ENTITY_NAME), optional(SPACES),
            // [at <coord>x<coord>]
            optional(AT_LITERAL + SPACES + record(INTEGER) + X_LITERAL + record(INTEGER)), SPACES_NEWLINE
    ),

    PART_OF_RELATION(
            // <entity_name> part of <entity_name>
            record(ENTITY_NAME), SPACES, PART_OF_LITERAL, SPACES, record(ENTITY_NAME), optional(SPACES),
            // [at <coord>x<coord>]
            optional(AT_LITERAL + SPACES + record(INTEGER) + X_LITERAL + record(INTEGER)), SPACES_NEWLINE
    ),

    RELATION(
            // <1|n|m> <entity_name> <relation>
            record(CARDINALITY), SPACES, record(ENTITY_NAME), SPACES, record(oneOrMore(WORD + optional(SPACES))), SPACES,
            // <1|n|m> <entity_name>
            record(CARDINALITY), SPACES, record(ENTITY_NAME), optional(SPACES),
            // [at <coord>x<coord>]
            optional(AT_LITERAL + SPACES + record(INTEGER) + X_LITERAL + record(INTEGER)),
            // \n <attributes>
            SPACES_NEWLINE, record(zeroOrMore(INDENTED_WORD))
    );

    // Generated match pattern for each grammar statement
    private final Pattern matchPattern;

    ERTRGrammar(String... matchers){
        StringBuilder buffer = new StringBuilder();
        Arrays.stream(matchers).forEach(buffer::append);
        matchPattern = Pattern.compile(buffer.toString());
    }

    @Override
    public int match(String src, int idx, ParserCallbacks.Success callback){
        Matcher matcher = matchPattern.matcher(src);

        if(matcher.find(idx) && matcher.start() == idx){
            // Extract content from all match groups
            List<String> tokens = IntStream.rangeClosed(1, matcher.groupCount())
                    .mapToObj(matcher::group)
                    .map(e -> (e != null && e.isEmpty())? null : e)
                    .collect(Collectors.toList());

            callback.accept(this, tokens);

            return matcher.end();
        }

        return -1;
    }
}
