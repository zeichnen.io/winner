package org.zeichner.winner.formatio.ertr;

import java.util.List;

public interface Grammar {
    /**
     * Is called when a match of this grammar against an input string
     * is attempted.
     *
     * @param src The <b>whole</b> source, not just the current token
     * @param callback This callback should be called if a string could
     *                 be successfully parsed.
     * @param idx The current index of the source. This is the index you
     *            shall start matching from.
     * @return The index of the last character matched or -1, if the
     * match failed.
     */
    int match(String src, int idx, ParserCallbacks.Success callback);

    /**
     * Calls {@link #match(String, int, ParserCallbacks.Success) match} using multiple callbacks.
     */
    default int callManyMatch(String src, int idx, List<ParserCallbacks.Success> callbacks){
        return match(src, idx, (a, b) ->
                callbacks.forEach(c -> c.accept(a, b))
        );
    }
}
