package org.zeichner.winner.formatio;

import javafx.stage.FileChooser;
import org.zeichner.winner.ui.fields.menufield.MenuField;
import org.zeichner.winner.ui.fields.textfield.TextField;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileOpener {

    private final FileChooser fileChooser;

    // region Simpleton-Stuff
    private final static FileOpener INSTANCE = new FileOpener();

    private FileOpener() {
        this.fileChooser = new FileChooser();
        this.fileChooser.getExtensionFilters().add(Extensions.wintxtExtensionFilter);
    }

    public static FileOpener getInstance() {
        return INSTANCE;
    }
    // endregion

    public void openFile() {

        Path pathToFile = this.fileChooser.showOpenDialog(MenuField.getInstance().getStage().getOwner()).toPath();
        try {
            String content = new String(Files.readAllBytes(pathToFile));
            TextField.getInstance().getCodeArea().replaceText(content);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
