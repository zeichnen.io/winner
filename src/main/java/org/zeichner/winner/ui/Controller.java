package org.zeichner.winner.ui;

import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import lombok.Getter;
import org.zeichner.winner.ui.fields.drawfield.DrawField;
import org.zeichner.winner.ui.fields.menufield.MenuField;
import org.zeichner.winner.ui.fields.textfield.TextField;

import java.awt.*;
import java.util.Optional;
import java.util.stream.Stream;

public class Controller {

    @FXML @Getter
    private HBox topBar;

    @FXML @Getter
    private AnchorPane canvas;

    @FXML @Getter
    private Pane textPane;

    @FXML @Getter
    private ContextMenu fileMenu;

    @FXML @Getter
    private Button fileBtn, minBtn, maxBtn, exitBtn;

    @FXML
    private void initialize() {
        // Initialize fields.
        Stream.of(
                DrawField.getInstance(),
                TextField.getInstance(),
                MenuField.getInstance()
        )
                .forEach(field -> field.setController(this));
    }

    // On calling this method the Scene and Stage in the @FXML-variables are set.
    public static void postInitialization() {
        MenuField.getInstance().addSceneItemFunctionalities();
    }

}
