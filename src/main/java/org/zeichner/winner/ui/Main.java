package org.zeichner.winner.ui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public final class Main extends Application {

    /**
     * CSS files to load on startup
     */
    private static final String[] CSS_FILES = {"text-style.css", "main-style.css", "erelement-style.css"};

    @Override
    public void start(Stage primaryStage) throws Exception {
        // Configure the window
        primaryStage.setTitle("WinnER");
        primaryStage.initStyle(StageStyle.UNDECORATED);

        // Load the FXML and CSS
        // TODO Find a nicer way to tell the user whats wrong
        URL fxml = getResource("app.fxml")
                .orElseThrow(() -> new RuntimeException("Could not find FXML file resource!"));
        Parent root = FXMLLoader.load(fxml);
        Scene scene = new Scene(root, 1200, 800);
        loadCss(scene);

        // Assign the scene to the window, call post-init callbacks and show the window
        primaryStage.setScene(scene);
        Controller.postInitialization();
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Returns an {@link Optional} containing the desired resource as an {@link URL}.
     * This method <b>shuts down the application</b> if the resource name is malformed!
     *
     * @return the desired resource or an empty {@link Optional} if the resource could
     * not be found.
     */
    public static Optional<URL> getResource(String name) {
        try {
            URL fxml = Main.class.getResource("src/main/resources/" + name);
            if (fxml == null) {
                fxml = Paths.get("src", "main", "resources", name).toUri().toURL();
            }

            return Optional.of(fxml);
        }
        catch (MalformedURLException e){
            // TODO Find a nicer way to tell the user whats wrong
            e.printStackTrace();
            System.exit(1);
        }
        // Never called
        return Optional.empty();
    }

    private static void loadCss(Scene scene){
        for(String file : CSS_FILES){
            // TODO Find a nicer way to tell the user whats wrong
            String resource = Main.getResource(file)
                    .orElseThrow(() -> new RuntimeException("Couldn't file CSS resource: " + file))
                    .toExternalForm();
            scene.getStylesheets().add(resource);
        }
    }
}
