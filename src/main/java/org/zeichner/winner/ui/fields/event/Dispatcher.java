package org.zeichner.winner.ui.fields.event;

import org.zeichner.winner.ui.fields.Field;

import java.util.HashSet;
import java.util.Set;

public final class Dispatcher {

    /**
     *  Contains all of the generated fields.
     */
    private final static Set<Field> fields = new HashSet<>();

    /**
     * Sends the event to everyone who is not the sender.
     *
     * @param sender The {@link Field} who sent the event.
     * @param event The {@link Event} that was triggered and should be sent.
     */
    public static void send(Field sender, Event event) {
        for(Field field : fields) {

            if(field != sender) {
                field.areYouInterestedInEvent(event);
            }

        }
    }

    /**
     * Automatically called when a Field is created.
     */
    public static void addField(Field field) {
        fields.add(field);
    }


}
