package org.zeichner.winner.ui.fields.erelements;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlendMode;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import javafx.scene.text.Font;
import lombok.Getter;
import org.zeichner.winner.ui.fields.drawfield.DrawField;
import org.zeichner.winner.ui.fields.erelements.connections.AConnection;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

import java.util.*;

@Getter
public abstract class ERElement<T extends Shape> extends Group {

    protected final Skeleton<T> skeleton;
    protected final ArrayList<Point2D> connectionPoints = new ArrayList<>();

    protected ERElement(Skeleton<T> skeleton, EROption... options) {
        super(skeleton.getBody(), skeleton.getText());
        this.skeleton = Objects.requireNonNull(skeleton,"The skeleton of an ERElement cannot be null.");
        this.setEREvents();
        this.setConnectionPoints();
        Arrays.stream(options)
                .forEach(option -> Objects.requireNonNull(option, "An EROption of an ERElement cannot be null.").useOn(this));
    }

    /**
     * This method can be used in the constructor of an ER-Element to define default values for it (e.g.: background-color, etc.)
     */
    public void setDefaultStyling() {

        TextField textField = this.getSkeleton().getText();
        T body = this.getSkeleton().getBody();

        // Setting "static" default-behaviour through css.
        textField.getStyleClass().add("ertext");
        body.getStyleClass().add("erbody");

        // Anything calculative is done below
        final double
                    textPrefWidth = 100,
                    textPrefHeight = 30;

        textField.setPrefWidth(textPrefWidth);
        textField.setPrefHeight(textPrefHeight);

        // Formula: (xPos) + (body_width/2) - (text_width/2) and (yPos) + (body_height/2) - (text_height/2)
        final double defaultTextX = (this.getSkeleton().getX()) + (this.getSkeleton().getWidth() / 2.0) - (textPrefWidth/2.0),
                     defaultTextY = (this.getSkeleton().getY()) + (this.getSkeleton().getHeight() / 2.0) - (textPrefHeight/2.0);

        textField.setLayoutX(defaultTextX);
        textField.setLayoutY(defaultTextY);

    }

    /**
     * The type-specific events are defined here.
     */
    protected abstract void setEREvents();

    /**
     * The type-specific connection-points are defined here.
     */
    protected abstract void setConnectionPoints();

    /**
     * Rotates a point by a certain amount around the center of the ERElement.
     *
     * @param point2D The point to rotate
     * @param degrees The amount of degress the point is rotated by
     * @return The rotated point.
     */
    protected Point2D getRotatedPoint(Point2D point2D, double degrees) {

        final double angle = Math.toRadians(degrees);

        final double cx = this.getSkeleton().getX() + (this.getSkeleton().getWidth() / 2.0);
        final double cy = this.getSkeleton().getY() + (this.getSkeleton().getHeight() / 2.0);

        final double tempX = point2D.getX() - cx;
        final double tempY = point2D.getY() - cy;

        return new Point2D(cx + tempX*Math.cos(angle) - tempY*Math.sin(angle), cy + tempX*Math.sin(angle) + tempY*Math.cos(angle));

    }

    /**
     * @param pointOfOther: The point of the other ERElement
     * @param other: The other ERElement
     * @return The closest point to the other ERElement
     */
    public Point2D getClosestPointTo(Point2D pointOfOther, ERElement<?> other) {

        return Collections.min(
                this.getConnectionPoints(),
                Comparator.comparing(
                        point -> {
                            Point2D translatedThisPoint = new Point2D(point.getX() + this.getTranslateX(), point.getY() + this.getTranslateY());
                            Point2D translatedOtherPoint = new Point2D(pointOfOther.getX() + other.getTranslateX(), pointOfOther.getY() + other.getTranslateY());

                            return translatedThisPoint.distance(translatedOtherPoint);
                        }
                )
        );

    }

    // region Basic-Element-Event-Methods

    /**
     * Properties that are necessary to change the position of an ER-Element by dragging it.
     */
    private double groupOriginalPosX, groupOriginalPosY;

    /**
     * Properties that are necessary to change the position of an ER-Element by dragging it.
     */
    private double groupTranslateX, groupTranslateY;


    // http://java-buddy.blogspot.com/2013/07/javafx-drag-and-move-something.html
    /**
     * When added to an ER-Element and called by mouse-pressed:
     * <p> - It's index-position inside the canvas will be set to last (= "selected"-effect). </p>
     * <p> - It's original and translated position will be saved. </p>
     */
    private EventHandler<MouseEvent> targetERElementEvent = event -> {
        if (event.getButton().equals(MouseButton.PRIMARY)) {
            groupOriginalPosX = event.getSceneX();
            groupOriginalPosY = event.getSceneY();
            groupTranslateX = ((ERElement<?>) event.getSource()).getTranslateX();
            groupTranslateY = ((ERElement<?>) event.getSource()).getTranslateY();
            DrawField.getInstance().getCanvas().getChildren().remove(event.getSource());
            DrawField.getInstance().getCanvas().getChildren().add((ERElement<?>) event.getSource());
        }

    };

    /**
     * When added to an ER-Element and called by mouse-dragged:
     * <p> - It's original position will be changed to the mouse's current position (with the corresponding offset). </p>
     */
    private EventHandler<MouseEvent> moveERElementEvent = event -> {
        if (EventHelper.isLeftButton(event)) {
            double offsetX = event.getSceneX() - groupOriginalPosX;
            double offsetY = event.getSceneY() - groupOriginalPosY;
            double newGroupTranslateX = groupTranslateX + offsetX;
            double newGroupTranslateY = groupTranslateY + offsetY;
            ((ERElement<?>) event.getSource()).setTranslateX(newGroupTranslateX);
            ((ERElement<?>) event.getSource()).setTranslateY(newGroupTranslateY);

            // Consumes the event, so it's not further forwarded to the canvas
            event.consume();
        }
    };

    /**
     * When added to an ER-Element and called by mouse-clicked:
     * <p> - The ER-Element will be deleted from the canvas.getChildren()-array. </p>
     */
    private EventHandler<MouseEvent> deleteERElementEvent = event -> {
        if (EventHelper.isMiddleButton(event)) {
            DrawField.getInstance().getCanvas().getChildren().remove(event.getSource());
        }
    };

    public void setDefaultEvents() {
        this.setOnMousePressed(targetERElementEvent);
        this.setOnMouseDragged(moveERElementEvent);
        this.setOnMouseClicked(deleteERElementEvent);
    }
    // endregion

    /**
     * Stores basic helping methods such as checking if on a {@link MouseEvent} a certain button has been pressed.
     */
    protected static class EventHelper {

        public static boolean isLeftButton(MouseEvent event) {
            return event.getButton().equals(MouseButton.PRIMARY);
        }

        public static boolean isRightButton(MouseEvent event) {
            return event.getButton().equals(MouseButton.SECONDARY);
        }

        public static boolean isMiddleButton(MouseEvent event) {
            return event.getButton().equals(MouseButton.MIDDLE);
        }

    }

}
