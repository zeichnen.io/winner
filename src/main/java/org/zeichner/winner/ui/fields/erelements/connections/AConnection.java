package org.zeichner.winner.ui.fields.erelements.connections;

import javafx.beans.property.DoubleProperty;
import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Shape;
import lombok.*;
import org.zeichner.winner.ui.fields.erelements.ERElement;
import org.zeichner.winner.ui.fields.erelements.Skeleton;
import org.zeichner.winner.ui.fields.erelements.entities.AEntity;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A AConnection is the "intermedia" point of a connection between two or more {@link org.zeichner.winner.ui.fields.erelements.entities.Entity entities}.
 */

@Getter
@Setter
public abstract class AConnection<T extends Shape> extends ERElement<T> {

    /**
     * Stores, which neighbours a AConnection has and from which point that neighbour connects to.
     */
    private final Map<AEntity<?>, PointAndPath> neighbours = new HashMap<>(); // The neighbours of a connection are always AEntities!

    AConnection(Skeleton<T> skeleton, AEntity<?> startingNeighbour, Point2D startingNeighbourPoint, EROption... options) {
        super(skeleton, options);
        this.addNeighbourConnection(startingNeighbour, startingNeighbourPoint);
    }

    /**
     * Refreshes the neighbour in order to get a new shortest path.
     *
     * <p> See https://gitlab.com/zeichnen.io/winner/issues/33 if you wonder, why this works! </p>
     *
     * @param neighbour The neighbour
     */
    public void useNewClosestPointPair(AEntity<?> neighbour) {

        this.refreshNeighbour(neighbour, neighbour.getClosestPointTo(this.getConnectionPoints().get(0), this));

    }

    @Override
    public void setDefaultEvents() {
        super.setDefaultEvents();

        Set<AEntity<?>> neighboursKeysCopy = new HashSet<>();

        this.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            neighboursKeysCopy.clear();
            neighboursKeysCopy.addAll(this.neighbours.keySet());
        });

        this.addEventHandler(MouseEvent.MOUSE_DRAGGED, event -> neighboursKeysCopy.forEach(this::useNewClosestPointPair));

    }

    /**
     * A inner class used to bundle the path combined with the point of the neighbour which the path connects to.
     */
    @Data
    @AllArgsConstructor
    private static class PointAndPath {

        private final Point2D neighbourPoint;
        private final Path path;

    }

    /**
     * @param neighbour The neighbour.
     * @return The point of the neighbour which the path connects to.
     */
    public Point2D getPoint2DToNeighbour(AEntity<?> neighbour) {
        return this.neighbours.get(neighbour).getNeighbourPoint();
    }

    /**
     * Adds a neighbour-connection by adding and drawing a {@link Path} to the canvas (whilst storing the neighbour-point)
     * as well as adding the neighbour to the neighbours-map
     *
     * @param neighbour The neighbour
     * @param pointOfNeighbour The point of the neighbour which is used to connect to the the Connection
     */
    private void addNeighbourConnection(AEntity<?> neighbour, Point2D pointOfNeighbour) {

        final Point2D fromPoint = this.getClosestPointTo(pointOfNeighbour, neighbour);
        final MoveTo from = new MoveTo(fromPoint.getX(), fromPoint.getY());
        final LineTo to = new LineTo(pointOfNeighbour.getX(), pointOfNeighbour.getY());

        to.xProperty().bind(neighbour.translateXProperty().add(pointOfNeighbour.getX()).subtract(this.translateXProperty()));
        to.yProperty().bind(neighbour.translateYProperty().add(pointOfNeighbour.getY()).subtract(this.translateYProperty()));

        Path path = new Path(from, to);

        this.getChildren().add(0, path);

        this.neighbours.put(
                neighbour,
                new PointAndPath(pointOfNeighbour, path)
        );

        neighbour.getConnections().add(this);

    }

    /**
     * Removes any every information a Connection has to an Entity (the path to it and the neighbour entry)
     * @param neighbour The neighbour
     */
    public void deleteNeighbourConnection(AEntity<?> neighbour) {
        this.getChildren().remove(this.neighbours.get(neighbour).path);   // Deleting the path to the neighbour
        this.neighbours.remove(neighbour);                                // Deleting the neighbour entry
        neighbour.getConnections().remove(this);                      // Delete self from connections-set of the neighbour
    }

    /**
     * Simply calls the {@link #deleteNeighbourConnection(AEntity)} and {@link #addNeighbourConnection(AEntity, Point2D)} methods one after another to refresh the entry.
     * @param neighbour The neighbour
     * @param pointOfNeighbour The point of the neighbour which is used to connect to the the Connection
     */
    private void refreshNeighbour(AEntity<?> neighbour, Point2D pointOfNeighbour) {
        this.deleteNeighbourConnection(neighbour);
        this.addNeighbourConnection(neighbour, pointOfNeighbour);
    }

}
