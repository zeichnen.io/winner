package org.zeichner.winner.ui.fields.erelements.cardinalities;

import lombok.Getter;

public enum Cardinality {

    NONE(""),
    ONE("1"),
    MANY_N("n"),
    MANY_M("m");

    @Getter
    private final String value;

    Cardinality(String value) {
        this.value = value;
    }


}
