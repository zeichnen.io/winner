package org.zeichner.winner.ui.fields.erelements.customshapes;

import javafx.geometry.Point2D;
import javafx.scene.shape.Polygon;

/**
 * A Triangle-Shape made by the Polygon-Class in order to simplify it.
 * This class is used as the body of the Is-A-Element
 */
public class Triangle extends Polygon {

    /** Default-Constructor to quickly get a new Triangle. */
    public Triangle() {
        super(100, 100, 150, 200, 50, 200);
    }

    /** Constructor which takes three points to form a new Triangle.  */
    public Triangle(Point2D p1, Point2D p2, Point2D p3) {
        super(p1.getX(), p1.getY(), p2.getX(), p2.getY(), p3.getX(), p3.getY());
    }

}
