package org.zeichner.winner.ui.fields.erelements.connections;

import javafx.geometry.Point2D;
import javafx.scene.shape.Shape;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.zeichner.winner.ui.fields.erelements.Skeleton;
import org.zeichner.winner.ui.fields.erelements.customshapes.Triangle;
import org.zeichner.winner.ui.fields.erelements.entities.AEntity;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

import java.util.Collections;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
public class IsA extends AConnection<Triangle> {

    public IsA(Skeleton<Triangle> skeleton, AEntity<? extends Shape> startingNeighbour, Point2D startingNeighbourPoint, EROption... options) {
        super(skeleton, startingNeighbour, startingNeighbourPoint, options);
        this.getSkeleton().getText().setTranslateY(20);
    }

    @Override
    public void setEREvents() {
    }

    @Override
    protected void setConnectionPoints() {

        List<Double> points = this.getSkeleton().getBody().getPoints();
        Collections.addAll(this.getConnectionPoints(),
                new Point2D(points.get(0), points.get(1)),
                new Point2D(points.get(2), points.get(3)),
                new Point2D(points.get(4), points.get(5))
        );

    }

}
