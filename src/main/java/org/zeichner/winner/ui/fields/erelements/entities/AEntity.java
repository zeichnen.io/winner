package org.zeichner.winner.ui.fields.erelements.entities;

import javafx.geometry.Point2D;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Shape;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.zeichner.winner.ui.fields.drawfield.DrawField;
import org.zeichner.winner.ui.fields.erelements.ERElement;
import org.zeichner.winner.ui.fields.erelements.Skeleton;
import org.zeichner.winner.ui.fields.erelements.connections.AConnection;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

@Getter
@Setter
public abstract class AEntity<T extends Shape> extends ERElement<T> {

    private final Set<AConnection<? extends Shape>> connections = new CopyOnWriteArraySet<>();

    AEntity(Skeleton<T> skeleton, EROption... options) {
        super(skeleton, options);
    }

    @Override
    public void setDefaultEvents() {

        super.setDefaultEvents();

        this.addEventHandler(MouseEvent.MOUSE_CLICKED, this::deleteNeighbourConnection);
        this.addEventHandler(MouseEvent.MOUSE_DRAGGED, this::recalculateConnectionPoints);

    }

    private void deleteNeighbourConnection(MouseEvent event) {
        if(EventHelper.isMiddleButton(event)) {
            this.connections.forEach(connection -> {
                connection.deleteNeighbourConnection(this);
                if(connection.getNeighbours().isEmpty()) {
                    DrawField.getInstance().getCanvas().getChildren().remove(connection);
                }
            });
        }
    }

    private void recalculateConnectionPoints(MouseEvent event) {
        if (EventHelper.isLeftButton(event)) {
            this.connections.forEach(connection -> connection.useNewClosestPointPair(this));
        }
    }


    @Override
    protected void setConnectionPoints() {
        double  x = this.getSkeleton().getX(),
                y = this.getSkeleton().getY(),
                w = this.getSkeleton().getWidth(),
                h = this.getSkeleton().getHeight();

        Collections.addAll(this.getConnectionPoints(),
                new Point2D(x + (w / 2.0), y),
                new Point2D(x + w, y + (h / 2.0)),
                new Point2D(x + (w / 2.0), y + h),
                new Point2D(x, y + (h / 2.0))
        );
    }

}
