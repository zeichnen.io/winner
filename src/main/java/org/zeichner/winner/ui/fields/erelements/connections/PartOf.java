package org.zeichner.winner.ui.fields.erelements.connections;

import javafx.geometry.Point2D;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.zeichner.winner.ui.fields.erelements.Skeleton;
import org.zeichner.winner.ui.fields.erelements.entities.AEntity;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

import java.util.Collections;

@EqualsAndHashCode(callSuper = true)
public class PartOf extends AConnection<Rectangle> {

    private final static int ROTATION = 45;

    public PartOf(Skeleton<Rectangle> skeleton, AEntity<? extends Shape> startingNeighbour, Point2D startingNeighbourPoint, EROption... options) {
        super(skeleton, startingNeighbour, startingNeighbourPoint, options);
        this.getSkeleton().getBody().setRotate(ROTATION);
    }

    @Override
    public void setEREvents() {
    }

    @Override
    protected void setConnectionPoints() {
        Collections.addAll(this.getConnectionPoints(),
                this.getRotatedPoint(new Point2D(this.getSkeleton().getX(), this.getSkeleton().getY()), ROTATION),
                this.getRotatedPoint(new Point2D(this.getSkeleton().getX() + this.getSkeleton().getWidth(), this.getSkeleton().getY()), ROTATION),
                this.getRotatedPoint(new Point2D(this.getSkeleton().getX(), this.getSkeleton().getY() + this.getSkeleton().getHeight()), ROTATION),
                this.getRotatedPoint(new Point2D(this.getSkeleton().getX() + this.getSkeleton().getWidth(), this.getSkeleton().getY() + this.getSkeleton().getHeight()), ROTATION)
        );
    }

}
