package org.zeichner.winner.ui.fields.menufield;

import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.Data;
import lombok.Getter;
import org.zeichner.winner.formatio.ERTRExporter;
import org.zeichner.winner.formatio.FileOpener;
import org.zeichner.winner.formatio.PNGExporter;

import java.io.File;

/**
 *  In here we simply handle the different {@link javafx.scene.control.MenuItem MenuItems} of our {@link javafx.scene.control.ContextMenu File-Tab},
 *  defining which set of methods should be executed on the individual MenuItems
 *
 *  These methods will be defined in Helper-Classes which are located in the {@link org.zeichner.winner.formatio Formatio-Package}
 */
@Getter
public final class FileTabManager {

    // region Simpleton-Stuff
    private final static FileTabManager INSTANCE = new FileTabManager();

    private FileTabManager() {}

    public static FileTabManager getInstance() {
        return INSTANCE;
    }
    // endregion

    private final MenuItem exportAsPNG = new MenuItem("Export as PNG");
    private final MenuItem exportAsText = new MenuItem("Export as Winner-Text");
    private final MenuItem openFile = new MenuItem("Open File");

    private Button fileMenuButton;
    private ContextMenu contextMenu;

    public void setFilemenuItems(Button fileMenuButton, ContextMenu contextMenu) {
        this.setFileMenuButton(fileMenuButton);
        this.setContextMenu(contextMenu);
        this.removeUnspecifiedActionMenuItem();
    }

    private void removeUnspecifiedActionMenuItem() {
        this.getContextMenu().getItems().remove(0);
    }
    private void setFileMenuButton(Button fileMenuButton) {
        this.fileMenuButton = fileMenuButton;
        this.getFileMenuButton().setOnMouseClicked(this.showFileMenuEvent);
    }
    private void setContextMenu(ContextMenu contextMenu) {
        this.contextMenu = contextMenu;
        this.addMenuItems();
        this.addMenuItemEvents();
    }

    // Show the file context menu
    private EventHandler<MouseEvent> showFileMenuEvent = event -> {
        Stage stage = MenuField.getInstance().getStage();
        this.getContextMenu().show(this.getFileMenuButton(),
                stage.getX() + this.getFileMenuButton().getLayoutX() + this.getFileMenuButton().getTranslateX(),
                stage.getY() + this.getFileMenuButton().getLayoutY() + this.getFileMenuButton().getTranslateX() + this.getFileMenuButton().getHeight());
    };

    private void addMenuItems() {
        this.getContextMenu().getItems().addAll(this.exportAsPNG, this.exportAsText, this.openFile);
    }

    private void addMenuItemEvents() {

        this.getExportAsPNG().setOnAction(event -> PNGExporter.getInstance().pngExport());
        this.getExportAsText().setOnAction(event -> ERTRExporter.getInstance().ertrExport());
        this.getOpenFile().setOnAction(event -> FileOpener.getInstance().openFile());

    }

}
