package org.zeichner.winner.ui.fields.erelements.entities;

import javafx.geometry.Point2D;
import javafx.scene.shape.Rectangle;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.zeichner.winner.ui.fields.erelements.Skeleton;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

import java.util.Collections;

@EqualsAndHashCode(callSuper = true)
public class Entity extends AEntity<Rectangle> {

    public Entity(Skeleton<Rectangle> skeleton, EROption... options) {
        super(skeleton, options);
    }

    @Override
    protected void setEREvents() {
    }

}
