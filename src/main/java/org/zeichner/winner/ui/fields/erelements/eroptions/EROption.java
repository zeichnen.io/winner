package org.zeichner.winner.ui.fields.erelements.eroptions;

import org.zeichner.winner.ui.fields.erelements.ERElement;

import java.util.function.Consumer;

public enum EROption {

    DEFAULT_STYLING(ERElement::setDefaultStyling),
    DEFAULT_EVENTS(ERElement::setDefaultEvents);

    private Consumer<ERElement<?>> action;

    EROption(Consumer<ERElement<?>> action) {
        this.action = action;
    }

    public void useOn(ERElement<?> erElement) {
        this.action.accept(erElement);
    }

}
