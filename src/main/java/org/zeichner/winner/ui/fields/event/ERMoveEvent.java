package org.zeichner.winner.ui.fields.event;

import lombok.Data;
import org.zeichner.winner.ui.fields.erelements.ERElement;

@Data
public final class ERMoveEvent implements Event {

    private final ERElement movedElement;

}
