package org.zeichner.winner.ui.fields.menufield;

import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.awt.*;
import java.util.Optional;

/**
 * In here we simply set the different Actions that should be executed whenever a "Window-Button" (Minimize-Button, Maximize-Button, Exit-Button) is pressed.
 */
@Data
public final class WindowManager {

    // region Simpleton-Stuff
    private final static WindowManager INSTANCE = new WindowManager();

    private WindowManager() {}

    public static WindowManager getInstance() {
        return INSTANCE;
    }
    // endregion

    private Button minimizeButton, maximizeButton, exitButton;
    private HBox topBar;

    /**
     * The position of Mouse when starting to drag the window used to move the window with an offset to the mouse.
     */
    private Point2D scenePoint;

    /**
     * Threshold when the mouse touches a panes' border.
     */
    private static final int MOUSE_THRESHOLD = 10;


    public void setWindowItems(Button minimizeButton, Button maximizeButton, Button exitButton, HBox topBar) {
        this.setMinimizeButton(minimizeButton);
        this.setMaximizeButton(maximizeButton);
        this.setExitButton(exitButton);
        this.setTopBar(topBar);
        this.changeCursorAtBorder();
    }

    private void setMinimizeButton(Button minimizeButton) {
        this.minimizeButton = minimizeButton;
        this.getMinimizeButton().setOnMouseClicked(this.minimizeEvent);
    }
    private void setMaximizeButton(Button maximizeButton) {
        this.maximizeButton = maximizeButton;
        this.getMaximizeButton().setOnMouseClicked(this.maximizeEvent);
    }
    private void setExitButton(Button exitButton) {
        this.exitButton = exitButton;
        this.getExitButton().setOnMouseClicked(this.closeEvent);
    }
    private void setTopBar(HBox topBar) {
        this.topBar = topBar;
        this.enableWindowDragging();
    }

    // Minimize the application
    private EventHandler<MouseEvent> minimizeEvent = event ->
            MenuField.getInstance().getStage().setIconified(!MenuField.getInstance().getStage().isIconified());

    // Maximize / normalize the application
    private EventHandler<MouseEvent> maximizeEvent = event ->
            MenuField.getInstance().getStage().setMaximized(!MenuField.getInstance().getStage().isMaximized());

    // Exit the application
    private EventHandler<MouseEvent> closeEvent = event -> System.exit(0);

    private void enableWindowDragging() {
        // Enable the decoration-less window to be dragged
        topBar.setOnMousePressed(event -> this.setScenePoint(new Point2D(event.getX(), event.getY())));
        topBar.setOnMouseDragged(event -> {
            Stage stage = MenuField.getInstance().getStage();
            stage.setMaximized(false); // Normalize the window size on drag

            Point mouse = MouseInfo.getPointerInfo().getLocation();

            stage.setX(mouse.getX() - this.getScenePoint().getX());
            stage.setY(mouse.getY() - this.getScenePoint().getY());
        });
    }

    private void changeCursorAtBorder() {
        // If the mouse touches the scenes' borders, change the cursor
        MenuField.getInstance().getStage().getScene().setOnMouseMoved(this::windowDraggingCursor);
    }

    /**
     * Returns the {@link Direction} where the cursor touches the stages' (or scenes') border.
     *
     * @return the side where the border is touched or an empty {@link Optional}.
     */
    private Optional<Direction> touches(double x, double y) {
        Stage stage = MenuField.getInstance().getStage();
        Direction direction;

        if (stage.isMaximized()) {
            direction = null;
        } // The window border cannot be touched while maximized
        else if (Math.abs(x) < MOUSE_THRESHOLD) {
            direction = Direction.LEFT;
        } else if (Math.abs(y) < MOUSE_THRESHOLD) {
            direction = Direction.UP;
        } else if (Math.abs(stage.getWidth() - x) < MOUSE_THRESHOLD) {
            direction = Direction.RIGHT;
        } else if (Math.abs(stage.getHeight() - y) < MOUSE_THRESHOLD) {
            direction = Direction.DOWN;
        } else {
            direction = null;
        }

        return Optional.ofNullable(direction);
    }

    private javafx.scene.Cursor getCursorByDirection(Direction d) {
        switch (d) {
            case LEFT:
            case RIGHT:
                return javafx.scene.Cursor.W_RESIZE;
            case UP:
            case DOWN:
                return javafx.scene.Cursor.N_RESIZE;
            default:
                throw new IllegalStateException();
        }
    }

    /**
     * Switch the mouse cursor based on where (or if) it touches the stages' (or scenes') border.
     */
    private void windowDraggingCursor(MouseEvent event) {
        javafx.scene.Cursor cursor = touches(event.getSceneX(), event.getSceneY()) // Get the side where the border is touched
                .map(this::getCursorByDirection) // Based on the side, determine the cursor
                .orElse(Cursor.DEFAULT); // Fallback the the default cursor if it doesn't touch the border

        MenuField.getInstance().getStage().getScene().setCursor(cursor);
    }

    /**
     * A generic side / direction enum.
     */
    private enum Direction {
        LEFT, RIGHT, UP, DOWN
    }

}
