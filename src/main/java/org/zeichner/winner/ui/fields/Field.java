package org.zeichner.winner.ui.fields;

import lombok.Getter;
import org.zeichner.winner.ui.Controller;
import org.zeichner.winner.ui.fields.event.Dispatcher;
import org.zeichner.winner.ui.fields.event.Event;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

public abstract class Field {

    /**
     * The Map that stores the {@link Event Events} a field is interested in as the keys, with the methods it should call when such an Event occurs
     * as the values.
     */
    @Getter
    protected final Map<Class<? extends Event>, Consumer<Event>> interestedEvents = new HashMap<>();

    /**
     * The central controller of the UI.
     */
    protected Controller controller;

    protected Field() {

        initialize();
        Dispatcher.addField(this);

    }

    /**
     * Checks if the sentEvent is instance of a class that the current Field is interested in.
     * If this is the case, the field will call the corresponding method where the parameters are delivered through the sentEvent parameter.
     *
     * @param sentEvent The event that was sent by one of the fields to the Dispatcher to tell the other fields about it.
     */
    public void areYouInterestedInEvent(Event sentEvent) {
        
        final Consumer<Event> method = this.interestedEvents.get(sentEvent.getClass());
        if(method != null) {
            // The field is interested in the event.
            method.accept(sentEvent);
        }

    }

    /**
     * Whenever an event happens inside a Field then this method should be called in order to notify the other Fields about that event.
     *
     * @param event The event that should be dispatched to the other Fields.
     */
    public void messageDispatcher(Event event) {
        Dispatcher.send(this, event);
    }

    /**
     * Returns the central controller of the UI.
     */
    public void setController(Controller controller){
        this.controller = controller;
    }

    /**
     * This method is automatically called in the constructor.
     *
     * It should be used to set the values of the {@link #interestedEvents Interested-Events-Map}.
     */
    public abstract void initialize(); // We add interested-events "inside" this method.

}
