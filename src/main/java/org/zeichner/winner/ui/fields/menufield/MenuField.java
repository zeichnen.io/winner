package org.zeichner.winner.ui.fields.menufield;

import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.zeichner.winner.ui.Controller;
import org.zeichner.winner.ui.fields.Field;

import java.awt.*;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Data
public final class MenuField extends Field {

    // region Simpleton-Stuff
    @Getter
    private final static MenuField instance = new MenuField();

    private MenuField() {
    }

    // endregion

    public void addSceneItemFunctionalities() {
        FileTabManager.getInstance().setFilemenuItems(controller.getFileBtn(), controller.getFileMenu());
        WindowManager.getInstance().setWindowItems(
                controller.getMinBtn(), controller.getMaxBtn(), controller.getExitBtn(), controller.getTopBar()
        );
    }

    public Stage getStage() {
        return (Stage) controller
                .getTopBar()
                .getScene()
                .getWindow();
    }

    @Override
    public void initialize() {

    }

}
