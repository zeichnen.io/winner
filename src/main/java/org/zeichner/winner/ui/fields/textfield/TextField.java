package org.zeichner.winner.ui.fields.textfield;

import javafx.application.Platform;
import lombok.Getter;
import org.fxmisc.flowless.VirtualizedScrollPane;
import org.fxmisc.richtext.LineNumberFactory;
import org.fxmisc.richtext.NavigationActions;
import org.fxmisc.richtext.model.StyleSpans;
import org.zeichner.winner.ui.Controller;
import org.zeichner.winner.ui.fields.Field;

import org.fxmisc.richtext.CodeArea;
import org.zeichner.winner.formatio.ertr.ERTRGrammar;
import org.zeichner.winner.formatio.ertr.Parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.zeichner.winner.formatio.ertr.LexerConstants.*;
import static org.zeichner.winner.formatio.ertr.LexerModifiers.*;
import static org.zeichner.winner.formatio.ertr.LexerModifiers.optional;

public final class TextField extends Field {

    private final CodeArea codeArea = new CodeArea();

    /**
     * The parser used for parsing the text representation (<code>ERTR</code>).
     */
    private final Parser parser = new Parser(ERTRGrammar.values());

    /**
     * A list where errors are saved to. All even number correspond to the start
     * index of an error, and all odd numbers to the end index of an error. As
     * such, this list contains either 0 or an even number of elements and is
     * always sorted.
     *
     * Example content:
     * <pre>
     *     3, 10, 15, 33
     * </pre>
     * Means everything between 3 and 10, 15 and 33 is invalid.
     */
    private final List<Integer> errorIdxs = new ArrayList<>();

    @Getter
    private static final TextField instance = new TextField();

    private TextField(){
        parser.getFailureCallbacks().add((idx, seq) -> {
            // Add the error idxs, see errorIdxs docs.
            errorIdxs.add(idx);
            errorIdxs.add(idx + seq.length());
        });

        getCodeArea().setParagraphGraphicFactory(LineNumberFactory.get(getCodeArea()));

        getCodeArea().textProperty().addListener(((observable, oldValue, newValue) -> {

            // Handle indentation
            String indentation = lastWordIndentation(newValue);
            if(!indentation.isEmpty() && newValue.length() > oldValue.length()){
                getCodeArea().appendText(indentation);
                Platform.runLater(() -> getCodeArea().end(NavigationActions.SelectionPolicy.CLEAR));
            }

            errorIdxs.clear(); // Clear all previous errors
            parser.parse(newValue); // Parse the newly pasted text
            int[] errors = errorIdxs.stream().mapToInt(i -> i).toArray(); // Determine the error idxs

            // Generate and combine highlighting spans
            StyleSpans<Collection<String>> highlighting = ERTRHighlighting.combine(
                    ERTRHighlighting.computeErrorHighlighting(newValue.length(), errors),
                    ERTRHighlighting.computeSyntaxHighlighting(newValue)
            );

            // Apply highlighting
            getCodeArea().setStyleSpans(0, highlighting);
        }));
    }

    @Override
    public void setController(Controller controller){
        super.setController(controller);
        controller
                .getTextPane()
                .getChildren()
                .add(new VirtualizedScrollPane<>(getCodeArea()));
    }

    @Override
    public void initialize() {
    }

    /**
     * Determines what indentation shall be append to the text, if any.
     *
     * @return The indentation or an empty string.
     */
    private String lastWordIndentation(String text){
        // Hacky but works
        Pattern pat = Pattern.compile(
                oneOf(
                        NEWLINE + record(SPACES) + WORD, // indented work

                        ENTITY_LITERAL + SPACES + ENTITY_NAME + optional(SPACES) + // entity
                        optional(AT_LITERAL + SPACES + INTEGER + X_LITERAL + INTEGER)
                ) + optional(SPACES) + NEWLINE_WITHOUT_EOF + "$"
        );

        Matcher mat = pat.matcher(text);
        if(mat.find()){
            String group = mat.group(1);
            return group == null? "  " :  group;
        }
        else { return ""; }
    }

    public CodeArea getCodeArea() {
        return codeArea;
    }
}
