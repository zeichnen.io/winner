package org.zeichner.winner.ui.fields.erelements;

import javafx.scene.control.TextField;
import javafx.scene.shape.Shape;
import lombok.Data;
import org.zeichner.winner.ui.fields.erelements.cardinalities.Cardinality;

import java.util.Objects;

/**
 * A skeleton is a "box" containing the main parts of an ERElement.
 * A skeleton currently consists of:
 *  - A body, which is a {@link Shape}
 *  - A text, which is a {@link javafx.scene.control.TextField}
 *  - A cardinality... which is a {@link Cardinality}
 *
 *  Due to the fact that these parts are bundled in this extern class, we can now add or remove parts of the skeleton
 *  without needing to update the constructors.
 *
 *  We are using templates to restrict the shape to a certain class, as all ERElements are not using the same shape.
 *
 */

@Data
public class Skeleton<T extends Shape> {

    private final T body;
    private final TextField text;
    private final Cardinality cardinality;

    public Skeleton(T body, TextField text, Cardinality cardinality) {
        this.body = Objects.requireNonNull(body, "The body of a Skeleton cannot be null.");
        this.text = Objects.requireNonNull(text, "The text of a Skeleton cannot be null.");
        this.cardinality = Objects.requireNonNull(cardinality, "The cardinality of a Skeleton cannot be null.");
    }

    public double getX() {
        return this.body.getBoundsInLocal().getMinX();
    }

    public double getY() {
        return this.body.getBoundsInLocal().getMinY();
    }

    public double getWidth() {
        return this.body.getBoundsInLocal().getWidth();
    }

    public double getHeight() {
        return this.body.getBoundsInLocal().getHeight();
    }
}
