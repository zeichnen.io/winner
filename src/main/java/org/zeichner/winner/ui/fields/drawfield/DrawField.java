package org.zeichner.winner.ui.fields.drawfield;

import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import lombok.Getter;
import org.zeichner.winner.ui.Controller;
import org.zeichner.winner.ui.fields.Field;
import org.zeichner.winner.ui.fields.erelements.ERElement;
import org.zeichner.winner.ui.fields.erelements.Skeleton;
import org.zeichner.winner.ui.fields.erelements.cardinalities.Cardinality;
import org.zeichner.winner.ui.fields.erelements.connections.Relation;
import org.zeichner.winner.ui.fields.erelements.entities.Entity;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

/**
 * This is one of the three main {@link Field Fields}.
 * <p>
 * This field is used as a "graphic"-display.
 * <p>
 * It includes an {@link javafx.scene.layout.AnchorPane AnchorPane} object which is used as the canvas.
 * <p>
 * Basic ER-Element-Events (such as creating, deleting and moving an ER-Element) are added to these Elements in this class. See: {@link DrawField#setCanvasProperties()}  Setting the canvas-properties}
 */
public final class DrawField extends Field {

    @Getter
    private final static DrawField instance = new DrawField();

    private DrawField() {}

    /**
     * The canvas; includes all ER-Elements inside an ObservableList which is accessible through the getChildren() method.
     */
    @Getter
    private AnchorPane canvas;

    /**
     * Called once inside the JavaFX {@link org.zeichner.winner.ui.Controller Controller} in order to set the canvas and it's properties ({@link #setCanvasProperties() Set Properties})
     *
     * @param controller the central UI controller
     */
    @Override
    public void setController(Controller controller){
        super.setController(controller);
        this.canvas = controller.getCanvas();
        this.setCanvasProperties();
    }

    /**
     * Defining default properties of a canvas, such as default children-event-handling (e.g.: creating, deleting)
     */
    private void setCanvasProperties() {

        this.canvas.setOnMouseClicked(event -> {

            if (event.getButton().equals(MouseButton.SECONDARY)) {


            }

        });

    }

    /**
     * Faster and more readable way to add an ER-Element to the canvas
     */
    private void addElementToCanvas(ERElement<?> drawElement) {

        this.canvas.getChildren().add(drawElement);

    }

    @Override
    public void initialize() {

    }

}
