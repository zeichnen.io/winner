package org.zeichner.winner.ui.fields.event;

import lombok.Data;
import org.zeichner.winner.ui.fields.erelements.ERElement;

@Data
public final class ERCreateEvent implements Event {

    private final ERElement createdElement;

}
