package org.zeichner.winner.ui.fields.erelements.entities;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.zeichner.winner.ui.fields.erelements.Skeleton;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

import java.util.Collections;

@EqualsAndHashCode(callSuper = true)
public class WeakEntity extends AEntity<Rectangle> {

    private final Rectangle innerRect;

    public WeakEntity(Skeleton<Rectangle> skeleton, EROption... options) {
        super(skeleton, options);
        this.innerRect = this.createInnerRect();
        this.setInnerRectProperties();
    }

    @Override
    public void setEREvents() {
    }

    private Rectangle createInnerRect() {
        final double indentation = 10;
        return new Rectangle(
                this.skeleton.getBody().getX() + indentation,
                this.skeleton.getBody().getY() + indentation,
                this.skeleton.getBody().getWidth() - (2 * indentation),
                this.skeleton.getBody().getHeight() - (2 * indentation)
        );
    }

    private void setInnerRectProperties() {
        this.innerRect.setMouseTransparent(true);
        this.innerRect.setFill(Color.TRANSPARENT);
        this.innerRect.setStroke(Color.BLACK);
        this.innerRect.setStrokeWidth(2);
        this.getChildren().add(innerRect);
    }


}
