package org.zeichner.winner.ui.fields;

import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.AnchorPane;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.Stage;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.api.FxRobot;
import org.testfx.assertions.api.Assertions;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;
import org.zeichner.winner.ui.Controller;
import org.zeichner.winner.ui.fields.drawfield.DrawField;
import org.zeichner.winner.ui.fields.erelements.ERElement;
import org.zeichner.winner.ui.fields.erelements.SEREC;
import org.zeichner.winner.ui.fields.erelements.connections.AConnection;
import org.zeichner.winner.ui.fields.erelements.connections.IsA;
import org.zeichner.winner.ui.fields.erelements.connections.PartOf;
import org.zeichner.winner.ui.fields.erelements.connections.Relation;
import org.zeichner.winner.ui.fields.erelements.customshapes.Triangle;
import org.zeichner.winner.ui.fields.erelements.entities.AEntity;
import org.zeichner.winner.ui.fields.erelements.entities.Entity;
import org.zeichner.winner.ui.fields.erelements.entities.WeakEntity;

import java.awt.*;
import java.io.IOException;
import java.util.Collections;

class DrawFieldIntegrationTest extends IntegrationTest{

    private AnchorPane canvas;

    private Entity entity1, entity2;
    private WeakEntity weakEntity1;
    private Relation relation;
    private IsA isA;
    private PartOf partOf;

    @Start
    public void start(Stage stage) throws IOException {

        DrawField.getInstance().setController(new TestController());
        canvas = DrawField.getInstance().getCanvas();

        stage.setScene(new Scene(canvas, 1000, 1000));
        stage.show();

        // region element-creation
        entity1 = new Entity(
                SEREC.simpleSkeleton(new Rectangle(100, 100, 100, 100)),
                SEREC.simpleEROptions()
        );

        entity2 = new Entity(
                SEREC.simpleSkeleton(new Rectangle(200, 100, 100, 100)),
                SEREC.simpleEROptions()
        );

        weakEntity1 = new WeakEntity(
                SEREC.simpleSkeleton(new Rectangle(100, 200, 100, 100)),
                SEREC.simpleEROptions()
        );

        relation = new Relation(
                SEREC.simpleSkeleton(new Rectangle(200, 200, 100, 100)),
                entity1,
                entity1.getConnectionPoints().get(2),
                SEREC.simpleEROptions()
        );

        isA = new IsA(
                SEREC.simpleSkeleton(new Triangle(new Point2D(500, 500), new Point2D(500, 600), new Point2D(400, 600))),
                entity2,
                entity2.getConnectionPoints().get(3),
                SEREC.simpleEROptions()
        );

        partOf = new PartOf(
                SEREC.simpleSkeleton(new Rectangle(300, 200, 100, 100)),
                weakEntity1,
                weakEntity1.getConnectionPoints().get(2),
                SEREC.simpleEROptions()
        );
        // endregion

        Collections.addAll(
                canvas.getChildren(),
                entity1,
                entity2,
                weakEntity1,
                relation,
                isA,
                partOf
        );

    }

    @Test
    public void select(FxRobot robot) {

        ERElement<?> nodeToSelect = (ERElement<?>) canvas.getChildren().get(0);

        robot
                .moveTo(nodeToSelect, new Point2D(-30, -30)) // Goto top-left corner
                .clickOn(MouseButton.PRIMARY); // click it

        // Check if the element is now selected --> if the position of the element has moved to the end of the array
        Assertions
                .assertThat(nodeToSelect)
                .isEqualTo(
                        canvas.getChildren().get(canvas.getChildren().size() - 1)
                );
    }

    @Test
    public void move(FxRobot robot) {

        AEntity<? extends Shape> entity = (AEntity<? extends Shape>) canvas.getChildren().get(0);
        AConnection<?> connectionOfEntity = entity.getConnections().toArray(new AConnection[]{})[0];

        final double oldX = entity.getTranslateX(),
                oldY = entity.getTranslateY();

        final Point2D pointToEntity = connectionOfEntity.getPoint2DToNeighbour(entity);
        final Point2D closestPoint = connectionOfEntity.getClosestPointTo(pointToEntity, entity);
        final Point2D oldPoint2D = new Point2D(closestPoint.getX(), closestPoint.getY());

        robot
                .moveTo(entity, new Point2D(-30, -30))
                .drag(MouseButton.PRIMARY)
                .dropBy(500, 500);

        // Check if the Entity has been moved
        Assertions.assertThat(oldX).isNotEqualTo(entity.getTranslateX());
        Assertions.assertThat(oldY).isNotEqualTo(entity.getTranslateY());

        Point2D newPoint = connectionOfEntity.getClosestPointTo(connectionOfEntity.getPoint2DToNeighbour(entity), entity);

        // Check if the connection has changed as well (We move the point to the opposite side of the Connection):
        Assertions.assertThat(oldPoint2D.getX()).isNotEqualTo(newPoint.getX());
        Assertions.assertThat(oldPoint2D.getY()).isEqualTo(newPoint.getY());


    }

    @Test
    public void delete(FxRobot robot) {
        final int oldSize = canvas.getChildren().size();
        Node nodeToDelete = canvas.getChildren().get(0); // A Connection is connected to this Entity --> when this Entity is deleted then the connection is deleted aswell
        robot
                .moveTo(nodeToDelete, new Point2D(-30, -30))
                .clickOn(MouseButton.MIDDLE);
        Assertions.assertThat(canvas.getChildren().size()).isEqualTo(oldSize - 2);
    }

}