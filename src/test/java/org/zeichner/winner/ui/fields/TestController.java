package org.zeichner.winner.ui.fields;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import lombok.Getter;
import org.zeichner.winner.ui.Controller;

/**
 * This class is used as an FXML-Controller-Simulation so that we can "access" everything FXML-related
 * from our Test-Classes
 */
public class TestController extends Controller {

    @Getter
    private HBox topBar = new HBox();

    @Getter
    private AnchorPane canvas = new AnchorPane();

    @Getter
    private Pane textPane = new Pane();

    @Getter
    private ContextMenu fileMenu = new ContextMenu();

    @Getter
    private Button
            fileBtn = new Button(),
            minBtn = new Button(),
            maxBtn = new Button(),
            closeBtn = new Button();

}
