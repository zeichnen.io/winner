package org.zeichner.winner.ui.fields.erelements;

import javafx.scene.control.TextField;
import javafx.scene.shape.Rectangle;
import org.junit.jupiter.api.Test;
import org.w3c.dom.css.Rect;
import org.zeichner.winner.ui.fields.IntegrationTest;
import org.zeichner.winner.ui.fields.erelements.cardinalities.Cardinality;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SkeletonTest extends IntegrationTest {

    @Test
    public void validConstructorTest() {

        Skeleton<Rectangle> skeleton = new Skeleton<>(
                new Rectangle(10, 20, 30, 40),
                new TextField("text"),
                Cardinality.NONE
        );

        assertThat(skeleton.getBody().getClass()).isEqualTo(Rectangle.class);
        assertThat(skeleton.getBody().getX()).isEqualTo(10);
        assertThat(skeleton.getBody().getY()).isEqualTo(20);
        assertThat(skeleton.getBody().getWidth()).isEqualTo(30);
        assertThat(skeleton.getBody().getHeight()).isEqualTo(40);

        assertThat(skeleton.getText().getText()).isEqualTo("text");

        assertThat(skeleton.getCardinality()).isEqualTo(Cardinality.NONE);
    }

    @Test
    public void invalidConstructorTest() {
        assertThrows(NullPointerException.class, () -> new Skeleton<Rectangle>(null, new TextField(), Cardinality.NONE));
        assertThrows(NullPointerException.class, () -> new Skeleton<Rectangle>(new Rectangle(), null, Cardinality.NONE));
        assertThrows(NullPointerException.class, () -> new Skeleton<Rectangle>(new Rectangle(), new TextField(), null));
    }

}
