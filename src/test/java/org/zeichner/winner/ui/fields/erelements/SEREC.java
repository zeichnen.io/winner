package org.zeichner.winner.ui.fields.erelements;

import javafx.geometry.Point2D;
import javafx.scene.control.TextField;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import org.zeichner.winner.ui.fields.erelements.cardinalities.Cardinality;
import org.zeichner.winner.ui.fields.erelements.customshapes.Triangle;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

/**
 *  This is a helping-class:
 *  It provides very simple ERElement-Object without the need of parameters.
 *  This class should be used for testing purposes only!
 */
public class SEREC { // Simple-ERElement-Creator

    public static TextField simpleText() {
        return new TextField("Hello!");
    }

    public static Cardinality simpleCardinality() {
        return Cardinality.NONE;
    }

    public static Rectangle simpleRectangle() {
        return new Rectangle(0, 0, 100, 100);
    }

    public static Triangle simpleTriangle() {
        return new Triangle(new Point2D(0, 100), new Point2D(50, 0), new Point2D(100, 100));
    }

    public static <T extends Shape> Skeleton<T> simpleSkeleton(T body) {
        return new Skeleton<>(
                body,
                simpleText(),
                simpleCardinality()
        );
    }

    public static EROption[] simpleEROptions() {
        return EROption.values();
    }

    /*
    public static Entity SIMPLE_ENTITY() {
        return new Entity(
                new Skeleton<>(
                        new Rectangle(0, 0, 100, 100),
                        new TextField("Entity"),
                        Cardinality.NONE
                ),
                EROption.DEFAULT_EVENTS, EROption.DEFAULT_STYLING
        );
    }


    public static WeakEntity SIMPLE_WEAK_ENTITY() {
        return new WeakEntity(
                new Skeleton<>(
                        new Rectangle(0, 0, 100, 100),
                        new TextField("Weak-Entity"),
                        Cardinality.NONE
                ),
                EROption.DEFAULT_EVENTS, EROption.DEFAULT_STYLING
        );
    }

    public static IsA SIMPLE_IS_A() {
        return new IsA(
                new Skeleton<>(
                        new Triangle(0, 100, 50, 0, 100, 100),
                        new TextField("Is-A"),
                        Cardinality.NONE
                ),
                EROption.DEFAULT_EVENTS, EROption.DEFAULT_STYLING
        );
    }

    public static PartOf SIMPLE_PART_OF() {
        return new PartOf(
                new Skeleton<>(
                        new Rectangle(0, 0, 100, 100),
                        new TextField("Part-Of"),
                        Cardinality.NONE
                ),
                EROption.DEFAULT_EVENTS, EROption.DEFAULT_STYLING
        );
    }

    public static Relation SIMPLE_RELATION() {
        return new Relation(
                new Skeleton<>(
                        new Rectangle(0, 0, 100, 100),
                        new TextField("Relation"),
                        Cardinality.NONE
                ),
                EROption.DEFAULT_EVENTS, EROption.DEFAULT_STYLING
        );
    }

    */

}
