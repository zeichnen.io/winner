package org.zeichner.winner.ui.fields.event;

import org.junit.jupiter.api.Test;
import org.zeichner.winner.ui.fields.Field;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DispatcherTest {

    private final static class DummyEvent1 implements Event {}

    private final static class DummyEvent2 implements Event {}

    private final static class DummyField extends Field {

        private static List<Event> recievedEvents = new ArrayList<>();


        @Override
        public void initialize() {

            interestedEvents.put(
                    DummyEvent1.class, event -> helloWorld((DummyEvent1) event)
            );

        }

        private void helloWorld(DummyEvent1 event){

            recievedEvents.add(event);

        }

        public static List<Event> getRecievedEvents() {
            return recievedEvents;
        }

    }

    @Test
    public void eventMessaging() {

        List<Event> expectedEvents = Arrays.asList(
                new DummyEvent1(),
                new DummyEvent1()
        );

        //Create 2 Dummy-Fields
        Field dummyField1 = new DummyField();
        Field dummyField2 = new DummyField();

        dummyField1.messageDispatcher(new DummyEvent2());
        dummyField1.messageDispatcher(expectedEvents.get(0));
        dummyField2.messageDispatcher(expectedEvents.get(1));

        assertThat(expectedEvents).containsExactlyInAnyOrderElementsOf(DummyField.getRecievedEvents());

    }


}
