package org.zeichner.winner.ui.fields.erelements;

import javafx.geometry.Point2D;
import javafx.scene.shape.Rectangle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
import org.zeichner.winner.ui.fields.IntegrationTest;
import org.zeichner.winner.ui.fields.erelements.connections.IsA;
import org.zeichner.winner.ui.fields.erelements.connections.PartOf;
import org.zeichner.winner.ui.fields.erelements.connections.Relation;
import org.zeichner.winner.ui.fields.erelements.customshapes.Triangle;
import org.zeichner.winner.ui.fields.erelements.entities.Entity;
import org.zeichner.winner.ui.fields.erelements.entities.WeakEntity;
import org.zeichner.winner.ui.fields.erelements.eroptions.EROption;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ERElementTest extends IntegrationTest {

    private Entity entity;
    private WeakEntity weakEntity;

    @Test
    public void validConstructorTest() {

        /*
         * None of these constructor-calls should throw an error.
         */
        entity = new Entity(SEREC.simpleSkeleton(SEREC.simpleRectangle()));
        weakEntity = new WeakEntity(SEREC.simpleSkeleton(SEREC.simpleRectangle()));
        new Relation(SEREC.simpleSkeleton(SEREC.simpleRectangle()), entity, entity.getConnectionPoints().get(0));
        new IsA(SEREC.simpleSkeleton(SEREC.simpleTriangle()), weakEntity, weakEntity.getConnectionPoints().get(0));
        new PartOf(SEREC.simpleSkeleton(SEREC.simpleRectangle()), entity, entity.getConnectionPoints().get(1));

    }

    @Test
    public void invalidConstructorTest() {

        Skeleton<Rectangle> validRectSkeleton = SEREC.simpleSkeleton(SEREC.simpleRectangle());
        Skeleton<Triangle> validTriSkeleton = SEREC.simpleSkeleton(SEREC.simpleTriangle());
        EROption validEROption = EROption.DEFAULT_EVENTS;

        Entity validNeighbour = new Entity(SEREC.simpleSkeleton(SEREC.simpleRectangle()));
        Point2D validNeighbourPoint = validNeighbour.getConnectionPoints().get(0);

        assertNullPointerEx(() -> new Entity(null, validEROption));
        assertNullPointerEx(() -> new Entity(validRectSkeleton, (EROption) null));

        assertNullPointerEx(() -> new WeakEntity(null, validEROption));
        assertNullPointerEx(() -> new WeakEntity(validRectSkeleton, (EROption) null));

        assertNullPointerEx(() -> new Relation(null, validNeighbour, validNeighbourPoint, validEROption));
        assertNullPointerEx(() -> new Relation(validRectSkeleton, null, validNeighbourPoint, validEROption));
        assertNullPointerEx(() -> new Relation(validRectSkeleton, validNeighbour, null, validEROption));
        assertNullPointerEx(() -> new Relation(validRectSkeleton, validNeighbour, validNeighbourPoint, (EROption) null));

        assertNullPointerEx(() -> new IsA(null, validNeighbour, validNeighbourPoint, validEROption));
        assertNullPointerEx(() -> new IsA(validTriSkeleton, null, validNeighbourPoint, validEROption));
        assertNullPointerEx(() -> new IsA(validTriSkeleton, validNeighbour, null, validEROption));
        assertNullPointerEx(() -> new IsA(validTriSkeleton, validNeighbour, validNeighbourPoint, (EROption) null));

        assertNullPointerEx(() -> new PartOf(null, validNeighbour, validNeighbourPoint, validEROption));
        assertNullPointerEx(() -> new PartOf(validRectSkeleton, null, validNeighbourPoint, validEROption));
        assertNullPointerEx(() -> new PartOf(validRectSkeleton, validNeighbour, null, validEROption));
        assertNullPointerEx(() -> new PartOf(validRectSkeleton, validNeighbour, validNeighbourPoint, (EROption) null));

    }

    private void assertNullPointerEx(Executable executable) {
        assertThrows(NullPointerException.class, executable);
    }


}
