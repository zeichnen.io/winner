package org.zeichner.winner.ui.fields;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;

@Tag("integration")
@ExtendWith(ApplicationExtension.class)
public abstract class IntegrationTest {
    static {
        if(System.getProperty("test.shellonly") != null) {
            // See https://github.com/TestFX/TestFX/issues/494
            System.setProperty("java.awt.headless", "true");
            System.setProperty("testfx.robot", "glass");
            System.setProperty("testfx.headless", "true");
            System.setProperty("prism.text", "t2k");
        }
    }
}
