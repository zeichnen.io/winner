package org.zeichner.winner.ui.fields;

import org.fxmisc.richtext.model.StyleSpan;
import org.fxmisc.richtext.model.StyleSpans;
import org.fxmisc.richtext.model.StyleSpansBuilder;
import org.junit.jupiter.api.Test;
import org.zeichner.winner.ui.fields.textfield.ERTRHighlighting;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class ERTRHighlightingTest {

    private static final String SYNTAX_ERROR_STYLE = "syntax_error";
    private static final String KEYWORD_STYLE = "keyword";
    private static final String CARDINALITY_STYLE = "cardinality";
    private static final String PRIMARY_KEY_STYLE = "primary_key";

    @Test
    public void stringWithNoHighlighting(){
        checkStyle("some random stuff", noStyle(17));
    }

    @Test
    public void keywordHighlightingWorks(){
        checkStyle("is a", keyword(4));
    }

    @Test
    public void cardinalityHighlightingWorks(){
        checkStyle("n 1 m",
                cardinality(1),
                noStyle(1),
                cardinality(1),
                noStyle(1),
                cardinality(1)
        );
    }

    @Test
    public void primaryKeyHighlightingWorks(){
        checkStyle("something\n\tpk_my_key",
                noStyle(11),
                primaryKey(9)
        );
    }

    @Test
    public void complexHighlightingWorks(){
        checkStyle("\t \tweak\t entity\n Xaxaxa at 30x30\npk_mykey\npk_\nn\r\n",
                noStyle(3),
                keyword(4),
                noStyle(2),
                keyword(6),
                noStyle(9),
                keyword(2),
                noStyle(7),
                primaryKey(8),
                noStyle(5),
                cardinality(1),
                noStyle(2)
        );
    }

    @Test
    public void simpleErrorHighlightingWorks(){
        // S = SyntaxError - = No Style
        // --SSS--S--
        // 0123456789
        checkErrors(10, new int[]{ 2, 5, 7, 8 },
                noStyle(2),
                syntaxError(3),
                noStyle(2),
                syntaxError(1),
                noStyle(2)
        );
    }

    @Test
    public void zeroLengthErrorHighlightingWorks(){
        spansEqual(
                ERTRHighlighting.computeErrorHighlighting(0),
                toSpans(noStyle(0))
        );
    }

    @Test
    public void invalidErrorHighlightingThrows(){
        assertThrows(IllegalArgumentException.class, () ->
            checkErrors(10, new int[]{1, 2, 3},
                    syntaxError(1),
                    noStyle(8))
        );
    }

    @Test
    public void simpleSpanCombinationWorks(){
        // K = Keyword S = SyntaxError - = No Style class 1 = KS
        // Test Data: --SSSSSS---
        //            KKKK------K
        // Result:    KK11SSSS--K

        checkCombine(
                // Expected
                toSpans(
                    keyword(2),
                    styleSpanOf(2, KEYWORD_STYLE, SYNTAX_ERROR_STYLE),
                    syntaxError(4),
                    noStyle(2),
                    keyword(1)
                ),

                // Test Data
                toSpans(
                    noStyle(2),
                    syntaxError(6),
                    noStyle(3)
                ),
                toSpans(
                    keyword(4),
                    noStyle(6),
                    keyword(1)
                )
        );
    }

    @Test
    public void unequalLengthCombinationThrows(){
        assertThrows(IllegalArgumentException.class, () ->
            checkCombine(
                    // Expected
                    toSpans(keyword(10)),
                    // Test Data
                    toSpans(noStyle(10)),

                    toSpans(noStyle(9))
            )
        );
    }

    @Test
    public void moreThanTwoCombinationWorks(){
        // K = Keyword S = SyntaxError C = Cardinality P = PrimaryKey
        // - = No Style class 1 = KS 2 = KC 3 = PS 4 = KPS 5 = KP
        // Test Data: KKKKKK---KKKKKK---
        //            SSS----SSSSSS-----
        //            ---CCC-PPPPPPPPPPP
        // Result:    111222-33444455PPP

        checkCombine(
                // Expected
                toSpans(
                    styleSpanOf(3, KEYWORD_STYLE, SYNTAX_ERROR_STYLE),
                    styleSpanOf(3, KEYWORD_STYLE, CARDINALITY_STYLE),
                    noStyle(1),
                    styleSpanOf(2, PRIMARY_KEY_STYLE, SYNTAX_ERROR_STYLE),
                    styleSpanOf(4, PRIMARY_KEY_STYLE, SYNTAX_ERROR_STYLE, KEYWORD_STYLE),
                    styleSpanOf(2, PRIMARY_KEY_STYLE, KEYWORD_STYLE),
                    primaryKey(3)
                ),

                // Test Data
                toSpans(
                    keyword(6),
                    noStyle(3),
                    keyword(6),
                    noStyle(3)
                ),

                toSpans(
                    syntaxError(3),
                    noStyle(4),
                    syntaxError(6),
                    noStyle(5)
                ),

                toSpans(
                    noStyle(3),
                    cardinality(3),
                    noStyle(1),
                    primaryKey(11)
                )
        );
    }

    @Test
    public void unequalSpanCountCombinationWorks(){
        // K = Keyword S = SyntaxError - = No Style class 1 = KS
        // Test Data: KKK---KKK
        //            SSSSS----
        // Result:    111SS-KKK

        checkCombine(
                // Expected
                toSpans(
                    styleSpanOf(3, KEYWORD_STYLE, SYNTAX_ERROR_STYLE),
                    syntaxError(2),
                    noStyle(1),
                    keyword(3)
                ),

                // Test Data
                toSpans(
                    keyword(3),
                    noStyle(3),
                    keyword(3)
                ),

                toSpans(
                    syntaxError(5),
                    noStyle(4)
                )
        );
    }

    @Test
    public void oneToManyCombinationWorks(){
        // K = Keyword S = SyntaxError - = No Style class 1 = KS
        // Test Data: KKKKKK
        //            SSS---
        // Result:    111KKK

        checkCombine(
                // Expected
                toSpans(
                    styleSpanOf(3, KEYWORD_STYLE, SYNTAX_ERROR_STYLE),
                    keyword(3)
                ),

                // Test Data
                toSpans(keyword(6)),

                toSpans(
                    syntaxError(3),
                    noStyle(3)
                )
        );
    }

    @Test
    public void oneToOneCombinationWorks(){
        // K = Keyword S = SyntaxError 1 = KS
        // Test Data: KKKKKK
        //            SSSSSS
        // Result:    111111

        checkCombine(
                // Expected
                toSpans(styleSpanOf(6, KEYWORD_STYLE, SYNTAX_ERROR_STYLE)),

                // Test Data
                toSpans(keyword(6)),

                toSpans(syntaxError(6))
        );
    }

    @SuppressWarnings("unchecked")
    @SafeVarargs
    private final void checkCombine(StyleSpans<Collection<String>> result, StyleSpans<Collection<String>>... allSpans){
        List<StyleSpans<Collection<String>>> spans = new ArrayList<>(Arrays.asList(allSpans));
        StyleSpans<Collection<String>> processed = ERTRHighlighting.combine(
                spans.remove(0),
                spans.toArray((StyleSpans<Collection<String>>[]) new StyleSpans[0])
        );

        spansEqual(result, processed);
    }

    @SafeVarargs
    private final StyleSpans<Collection<String>> toSpans(StyleSpan<Collection<String>>... styles){
        StyleSpansBuilder<Collection<String>> builder = new StyleSpansBuilder<>(styles.length);
        Arrays.stream(styles).forEach(builder::add);
        return builder.create();
    }

    @SafeVarargs
    private final void checkStyle(String src, StyleSpan<Collection<String>>... styles){
        check(ERTRHighlighting.computeSyntaxHighlighting(src), styles);
    }

    @SafeVarargs
    private final void checkErrors(int length, int[] errIdxs, StyleSpan<Collection<String>>... styles){
        check(ERTRHighlighting.computeErrorHighlighting(length, errIdxs), styles);
    }

    private void check(StyleSpans<Collection<String>> result, StyleSpan<Collection<String>>[] styles){
        StyleSpansBuilder<Collection<String>> builder = new StyleSpansBuilder<>();

        Arrays.stream(styles).forEach(builder::add);

        check(result, builder.create());
    }

    private void check(StyleSpans<Collection<String>> result, StyleSpans<Collection<String>> styles){
        assertEquals(result.length(), styles.length());

        assertEquals(result.getSpanCount(), styles.getSpanCount());

        spansEqual(result, styles);
    }

    private StyleSpan<Collection<String>> syntaxError(int length){
        return styleSpanOf(length, SYNTAX_ERROR_STYLE);
    }

    private StyleSpan<Collection<String>> keyword(int length){
        return styleSpanOf(length, KEYWORD_STYLE);
    }

    private StyleSpan<Collection<String>> cardinality(int length){
        return styleSpanOf(length, CARDINALITY_STYLE);
    }

    private StyleSpan<Collection<String>> primaryKey(int length){
        return styleSpanOf(length, PRIMARY_KEY_STYLE);
    }

    private StyleSpan<Collection<String>> noStyle(int length){
        return new StyleSpan<>(Collections.emptyList(), length);
    }

    private StyleSpan<Collection<String>> styleSpanOf(int length, String... cssClasses){
        return new StyleSpan<>(Arrays.asList(cssClasses), length);
    }

    private void spansEqual(StyleSpans<Collection<String>> a, StyleSpans<Collection<String>> b){
        if(a.getSpanCount() != b.getSpanCount()){
            fail(String.format("Span count not equal: %s vs %s,%n%s vs%n%s",
                    a.getSpanCount(), b.getSpanCount(), a, b));
        }

        for(int i = 0; i < a.getSpanCount(); i++){
            if(!spanEqual(a.getStyleSpan(i), b.getStyleSpan(i))){
                fail(String.format("Subspans (Index %d) not equal: %n%s equals %n%s",
                        i, a.getStyleSpan(i), b.getStyleSpan(i)));
            }
        }
    }

    private boolean spanEqual(StyleSpan<Collection<String>> a, StyleSpan<Collection<String>> b){
        return a.getLength() == b.getLength() &&
                a.getStyle().containsAll(b.getStyle()) &&
                b.getStyle().containsAll(a.getStyle());
    }
}
