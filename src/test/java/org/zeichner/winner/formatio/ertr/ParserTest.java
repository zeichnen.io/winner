package org.zeichner.winner.formatio.ertr;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;

public class ParserTest {
    // Dummy grammar parses any 2 numbers as a token
    private static final Grammar[] dummyGrammar = new Grammar[1];
    static {
        Pattern tokenPattern = Pattern.compile("[0-9]{2}");
        dummyGrammar[0] = (src, idx, callback) -> {
            Matcher m = tokenPattern.matcher(src);
            if(m.find(idx) && m.start() == idx){
                callback.accept(dummyGrammar[0], Collections.singletonList(m.group()));
                // idx | idx + 1 | idx + 2
                // '5' |   '3'   |   ' '
                return idx + 2;
            }

            return -1;
        };
    }

    private Parser parser;
    private StubCallbacks.Success successCallback;
    private StubCallbacks.Failure failureCallback;

    @BeforeEach
    public void beforeEach(){
        parser = new Parser(dummyGrammar);
        parser.getSuccessCallbacks().add(successCallback = new StubCallbacks.Success());
        parser.getFailureCallbacks().add(failureCallback = new StubCallbacks.Failure());
    }

    @Test
    public void singleTokenCanBeParsed(){
        parser.parse("12");

        assertExactlyResults("12");
        assertNoErrors();
    }

    @Test
    public void multipleTokensCanBeParsed(){
        parser.parse("12 34 56");

        assertExactlyResults("12", "34", "56");
        assertNoErrors();
    }

    @Test
    public void paddingIsIgnoredWhenParsing(){
        parser.parse(" \t12 34  \t56  ");

        assertExactlyResults("12", "34", "56");
        assertNoErrors();
    }

    @Test
    public void parsingDoesNotFailOnFailure(){
        parser.parse("12 an error 34 this is");

        assertThat(failureCallback.invalidSeqs).containsExactly("an error", "this is");
        assertThat(failureCallback.startIdxs).containsExactly(3, 15);

        assertExactlyResults("12", "34");
    }

    @Test
    public void multipleSuccessCallbacksAreCalled(){
        StubCallbacks.Success mySuccessCallback = new StubCallbacks.Success();
        parser.getSuccessCallbacks().add(mySuccessCallback);

        parser.parse("12");

        assertExactlyResults("12");
        assertExactlyResults(mySuccessCallback, "12");
        assertNoErrors();
    }

    @Test
    public void multipleFailureCallbacksAreCalled(){
        StubCallbacks.Failure myFailureCallback = new StubCallbacks.Failure();
        parser.getFailureCallbacks().add(myFailureCallback);

        parser.parse("err");

        assertThat(failureCallback.invalidSeqs).containsExactly("err");
        assertThat(failureCallback.startIdxs).containsExactly(0);

        assertThat(myFailureCallback.invalidSeqs).containsExactly("err");
        assertThat(myFailureCallback.startIdxs).containsExactly(0);

        assertNoTokens();
    }

    private void assertNoErrors(){
        assertThat(failureCallback.invalidSeqs).isEmpty();
        assertThat(failureCallback.startIdxs).isEmpty();
    }

    private void assertNoTokens(){
        assertThat(successCallback.grammars).isEmpty();
        assertThat(successCallback.lexedList).isEmpty();
    }

    public void assertExactlyResults(String... tokens){
        assertExactlyResults(successCallback, tokens);
    }

    public void assertExactlyResults(StubCallbacks.Success callback, String... tokens){
        assertThat(callback.lexedList).hasSize(tokens.length);
        assertThat(callback.lexedList).extracting(l -> l.get(0)).containsExactly(tokens);
    }
}
