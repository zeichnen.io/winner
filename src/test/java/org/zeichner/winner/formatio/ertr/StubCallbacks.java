package org.zeichner.winner.formatio.ertr;

import java.util.LinkedList;
import java.util.List;

/**
 * Stub {@link ParserCallbacks} recording the passed arguments.
 */
public class StubCallbacks {
    public static class Success implements ParserCallbacks.Success {
        public List<Grammar> grammars = new LinkedList<>();
        public List<List<String>> lexedList = new LinkedList<>();

        @Override
        public void accept(Grammar grammar, List<String> lexed) {
            this.grammars.add(grammar);
            this.lexedList.add(lexed);
        }
    }

    public static class Failure implements ParserCallbacks.Failure {
        public List<Integer> startIdxs = new LinkedList<>();
        public List<String> invalidSeqs = new LinkedList<>();

        @Override
        public void accept(Integer startIdx, String invalidSeq) {
            this.startIdxs.add(startIdx);
            this.invalidSeqs.add(invalidSeq);
        }
    }
}
