package org.zeichner.winner.formatio.ertr;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ERTRGrammarTest {
    private StubCallbacks.Success callback;

    @BeforeEach
    public void beforeEach(){
        callback = new StubCallbacks.Success();
    }

    @Test
    public void matchesOnlyFirstIsARelation(){
        int pos = ERTRGrammar.IS_A_RELATION.match("A is a B\nC is a G", 0, callback);

        assertEquals(9, pos);
        assertCallback(ERTRGrammar.IS_A_RELATION, "A", "B", null, null);
    }

    @Test
    public void advancedMatchesOfIsAWorkCorrectly(){
        int pos = ERTRGrammar.IS_A_RELATION.match("A is a B C is a G at 031x531", 9, callback);

        assertEquals(28, pos);
        assertCallback(ERTRGrammar.IS_A_RELATION, "C", "G", "031", "531");
    }

    @Test
    public void canMatchSimpleEntityCorrectly(){
        int pos = ERTRGrammar.ENTITY.match("entity MyEntity", 0, callback);

        assertEquals(15, pos);
        assertCallback(ERTRGrammar.ENTITY, null, "MyEntity", null, null, null);
    }

    @Test
    public void canMatchComplexEntityCorrectly(){
        String src = "weak entity \tAnEntity  at 12x312 \n prop1\n\tprop2 \nnotprop";
        int pos = ERTRGrammar.ENTITY.match(src, 0, callback);

        assertEquals(49, pos);
        assertCallback(ERTRGrammar.ENTITY, "weak", "AnEntity", "12", "312", " prop1\n\tprop2 \n");
    }

    @Test
    public void canMatchSimplePartOfRelationCorrectly(){
        int pos = ERTRGrammar.PART_OF_RELATION.match("Alpha part of Beta", 0, callback);

        assertEquals(18, pos);
        assertCallback(ERTRGrammar.PART_OF_RELATION, "Alpha", "Beta", null, null);
    }

    @Test
    public void canMatchComplexPartOfRelationCorrectly(){
        int pos = ERTRGrammar.PART_OF_RELATION.match("Alpha part of Beta at 991x12", 0, callback);

        assertEquals(28, pos);
        assertCallback(ERTRGrammar.PART_OF_RELATION, "Alpha", "Beta", "991", "12");
    }

    @Test
    public void canMatchSimpleRelationCorrectly(){
        int pos = ERTRGrammar.RELATION.match("1 Fahrzeug hat n Sitz", 0, callback);

        assertEquals(21, pos);
        assertCallback(ERTRGrammar.RELATION, "1", "Fahrzeug", "hat", "n", "Sitz", null, null, null);
    }

    @Test
    public void canMatchComplexRelationCorrectly(){
        String src = "n Mitarbeiter \tarbeiten  fuer manche m\t\tBoss at 10x30 \n prop1 \n  prop2\nnotprop";
        int pos = ERTRGrammar.RELATION.match(src, 0, callback);

        assertEquals(71, pos);
        assertCallback(ERTRGrammar.RELATION, "n", "Mitarbeiter", "arbeiten  fuer manche", "m", "Boss", "10", "30", " prop1 \n  prop2\n");
    }

    @Test
    public void matchFailWorksCorrectly(){
        int pos = ERTRGrammar.PART_OF_RELATION.match("Alpha part error of Beta", 0, callback);

        assertEquals(-1, pos);
        assertThat(callback.grammars).isEmpty();
        assertThat(callback.lexedList).isEmpty();
    }

    private void assertCallback(Grammar grammar, String... strings){
        assertThat(callback.grammars).first().isEqualTo(grammar);
        assertThat(callback.lexedList).first().asList().containsExactly((Object[]) strings);
    }
}
